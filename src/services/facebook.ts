import { Injectable  } from '@angular/core';
import { Facebook } from 'ionic-native';

@Injectable()
export class FacebookClient {
    
    getFriends(next){
        var path = '/me/friends?limit=25';
        if (next) path += `&after=${next}`
        return Facebook.api(path, ['user_friends']);
    }
        
}