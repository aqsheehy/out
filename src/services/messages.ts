import { Injectable  } from '@angular/core';
import { DataService } from './data';

@Injectable()
export class MessageFactory {
    
    constructor(public database: DataService) { }
    
    sharePhoto(targetEventId, photoUrl, date) {
        return {
            type: 'photo_shared',
            fromUid: this.database.uid,
            eventId: targetEventId,
            url: photoUrl,
            created_date: date
        };
    }
    
    userInvited(fromEventId, targetEventId?:string){
        return {
            type: 'user_invited',
            fromUid: this.database.uid,
            fromEventId: fromEventId,
            targetEventId: targetEventId
        };
    }
    
}

export interface ISink {
    process(message)
}

@Injectable()
export class MessageProcessor {
    
    sinks:Array<ISink>;
    
    constructor(public database: DataService) {
        
        this.sinks = [];
        this.database.ready(() => this.subscribe());
            
    }
    
    subscribe() {
        
        var messages = this.database.getMessages();
        
        messages.subscribe(newMessages => {
                
            if (!this.sinks.length) return;
            if (!newMessages.length) return;
            
            newMessages.forEach((msg) => {
                this.sinks.forEach(sink => {
                    sink.process(msg);
                })
            });
            
            messages.remove(); 
        });
    }
    
    attachSink(fn:ISink){
        this.sinks.push(fn);
    }
    
    detatchSink(fn:ISink){
        var index = this.sinks.indexOf(fn);
        if (index != -1) this.sinks.splice(index, 1);
    }
    
}

