import { Inject, Injectable  } from '@angular/core';
import { FirebaseApp } from 'angularfire2';

@Injectable()
export class FileService {

    uid: string;
    storage: any;
    
    constructor (@Inject(FirebaseApp) firebase: any){
        this.storage = firebase.storage();
    }
    
    uploadBase64(eventId, photoKey, imageData){
        return this.storage
          .ref(`users/${this.uid}/albums/${eventId}/photos/${photoKey}`)
          .putString(imageData, 'base64');
    }
    
    uploadBlob(eventId, photoKey, blob){
        return this.storage
          .ref(`users/${this.uid}/albums/${eventId}/photos/${photoKey}`)
          .put(blob);
    }

}