import { Platform } from 'ionic-angular';
import { Injectable  } from '@angular/core';
import { Facebook } from 'ionic-native';

import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

declare let firebase: any;

@Injectable()
export class AuthService {
    
    constructor(public platform: Platform, public af: AngularFire) {}

    login() {
        
        if(this.platform.is('cordova')) {
                
            Facebook.login(['email'])
                .then(response => { 
                    let creds = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
                    let providerConfig = {
                        provider: AuthProviders.Facebook,
                        method: AuthMethods.OAuthToken,
                        remember: 'default',
                        scope: ['email'],
                    };
                    this.af.auth.login(creds, providerConfig);
                });
            
        } else {
            
            let config = {
                provider: AuthProviders.Facebook,
                method: AuthMethods.Redirect,
                remember: 'default',
                scope: ['email']
            };
            this.af.auth.login(config);
            
        }
    }
}
