import { Injectable  } from '@angular/core';
import { Camera } from 'ionic-native';

@Injectable()
export class PhotoService {
    
    takePhoto() {
        return Camera.getPicture({ 
                quality: 100, 
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType : Camera.PictureSourceType.CAMERA,
                encodingType: Camera.EncodingType.JPEG
            })
            .then(imageDataOrUrl => this.preProcessImage(imageDataOrUrl));
    }
    
    choosePhoto() {
        return Camera.getPicture({ 
                quality: 100, 
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
                encodingType: Camera.EncodingType.JPEG
            })
            .then(imageDataOrUrl => this.preProcessImage(imageDataOrUrl))
    }
    
    private preProcessImage(imageDataOrUrl:string) {
        
        var isFileUrl = imageDataOrUrl.indexOf('file://') == 0;
        var photo:any = { takenWhen: new Date().getTime() };
        
        if (isFileUrl) {
            photo.fileUrl = imageDataOrUrl;
            photo.file = new File([], imageDataOrUrl);
        }
        else {
            photo.dataUrl = `data:image/jpeg;base64,${imageDataOrUrl}`;
            photo.data = imageDataOrUrl;
        }
        
        return photo;
    }
    
}