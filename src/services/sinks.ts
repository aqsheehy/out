import { Injectable  } from '@angular/core';
import { DataService } from './data';
import { ISink, MessageFactory } from './messages';
import 'rxjs/add/operator/map';


@Injectable()
export class PhotoSharedSink implements ISink {
    
    constructor(public database: DataService, public messages: MessageFactory) {}
    
    process(message){        
        return this.userIsInEvent(message.fromUid, message.eventId).then(allowed => {
            if (!allowed) return;
            
            var photos = this.database.getEventPhotos(message.eventId);
            photos.push({ created_date: message.created_date, photo_url: message.photo_url });
            
            var event = this.database.getEvent(message.eventId);
            event.$ref.child('/num_photos').transaction(num_photos => num_photos + 1);
        });
    }
    
    private userIsInEvent(uid, eventId){
        return new Promise((resolve, reject) => {
            this.database.getEventMembers(eventId).subscribe(members => {
                var fromMember = members.filter(i => i.uid == uid)[0];
                resolve(!!fromMember);                
            });
        });
    }
    
}

@Injectable()
export class UserInvitedSink implements ISink {
    
    userLookup:any;
    
    constructor(public database: DataService, public messages: MessageFactory) {
        this.userLookup = {};
    }
    
    process(message){
        return new Promise((resolve, reject) => {
            
            if (message.targetEventId){
                this.database.getEventMembers(message.targetEventId).push({
                    uid: message.fromUid,
                    eventId: message.fromEventId
                });
            }
            
            return resolve();
            
        });
    }
    
}