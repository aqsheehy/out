import { Injectable  } from '@angular/core';
import { AngularFire } from 'angularfire2';
import "rxjs/add/operator/map";

@Injectable()
export class DataService {
    
    uid: string;
    
    constructor(public af: AngularFire) { }
    
    ready(fn) {
        this.af.auth.subscribe(auth => {
            if (!auth) return;
            this.uid = auth.uid;
            fn(auth);
        });
    }
    
    getEventPhotos(eventId) {
        var ref = `/users/${this.uid}/albums/${eventId}/photos`;
        var photos = this.af.database.list(ref);
        return photos;
    }
    
    getEventPhoto(eventId, photoId){
        var ref = `/users/${this.uid}/albums/${eventId}/photos/${photoId}`;
        var photos = this.af.database.object(ref);
        return photos;
    }
    
    getEvent(eventId){
        var ref = `/users/${this.uid}/events/${eventId}`;
        var event = this.af.database.object(ref);
        return event;
    }
    
    getEventMembers(eventId){
        var ref = `/users/${this.uid}/events/${eventId}/members`;
        var event = this.af.database.list(ref);
        return event;
    }
    
    getEvents(){
        var ref = `/users/${this.uid}/events`;
        var events = this.af.database.list(ref);
        return events;
    }
    
    getMessages(uid?:string){
        var ref = `/users/${uid || this.uid}/messages`;
        return this.af.database.list(ref);
    }
    
}