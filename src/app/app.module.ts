import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { EventPage } from '../pages/event/event';
import { PicturePage } from '../pages/picture/picture';

import { AngularFireModule } from 'angularfire2';

import { DataService } from '../services/data';
import { FileService } from '../services/files';
import { MessageProcessor, MessageFactory } from '../services/messages';

// AF2 Settings
export const firebaseConfig = {
  apiKey: "AIzaSyD-cMv8UZ9U_MDvoFwJUScNy_9SImq0x2k",
  authDomain: "fluted-oath-134214.firebaseapp.com",
  databaseURL: "https://fluted-oath-134214.firebaseio.com",
  storageBucket: "fluted-oath-134214.appspot.com",
  messagingSenderId: "594577717810"
};

@NgModule({
  declarations: [ MyApp, HomePage, EventPage, PicturePage ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [ MyApp, HomePage, EventPage, PicturePage ],
  providers: [    
    { provide: ErrorHandler, useClass: IonicErrorHandler},
    DataService, FileService, MessageProcessor, MessageFactory
  ]
})
export class AppModule {}
