import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { AngularFire } from 'angularfire2';

import { HomePage } from '../pages/home/home';

import { MessageProcessor } from '../services/messages';
import { PhotoSharedSink, UserInvitedSink } from '../services/sinks'
import { AuthService } from '../services/auth';

@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`,
  providers: [PhotoSharedSink, UserInvitedSink, AuthService]
})
export class MyApp {
  
  rootPage = HomePage;

  constructor(platform: Platform, af: AngularFire, messaging: MessageProcessor, sink1:PhotoSharedSink, sink2:UserInvitedSink, authService:AuthService) {
    platform.ready().then(() => {
      
      StatusBar.styleDefault();
      Splashscreen.hide();
      
      messaging.attachSink(sink1);
      messaging.attachSink(sink2);
      
      // Login
      af.auth.subscribe(auth => {
          if (!auth) authService.login();
      });
      
    });
  }
}
