import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { EventPage } from '../event/event';

import { DataService } from '../../services/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  events: any
  searchQuery: string;

  constructor(public navCtrl: NavController, public database: DataService) {
    this.searchQuery = '';
    
    database.ready(() => {
      this.events = database.getEvents().map(array => array.reverse());
    });
  }
 
  viewEvent(evt) {
    this.navCtrl.push(EventPage, { eventid: evt.$key });
  }
  
  createEvent() {
    var newEvent = { num_photos: 0, active: true, created_date: new Date().getTime() };
    this.events.push(newEvent);
  }
  
  onInput($event){}
  onCancel($event){}
 
}
