import { Component } from '@angular/core';
import { NavParams, ViewController  } from 'ionic-angular';
import { DataService } from '../../services/data';

@Component({
  selector: 'page-picture',
  templateUrl: 'picture.html'
})
export class PicturePage {
    
    photo: any; 
    
    constructor (public params: NavParams, public database: DataService, public viewCtrl: ViewController){
               
        database.ready(() => {
            var pictureid = params.get('pictureid');
            var eventid = params.get('eventid');
            this.photo = database.getEventPhoto(eventid, pictureid);
        });
        
    }
    
    dismiss() {
        this.viewCtrl.dismiss();
    }
    
    
}