import { Component } from '@angular/core';

import { NavParams, ModalController } from 'ionic-angular';

import { DataService } from '../../services/data';
import { FileService } from '../../services/files';
import { MessageFactory } from '../../services/messages';

import { PhotoService } from '../../services/photos';
import { FacebookClient } from '../../services/facebook';

import { PicturePage } from '../picture/picture';

@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
  providers: [PhotoService, FacebookClient]
})
export class EventPage {

  event: any;
  photos: any;
  
  constructor(public params: NavParams, public modal: ModalController, public database: DataService, public storage: FileService, public camera: PhotoService, public messages: MessageFactory) {
    
    database.ready(() => {
      var id = params.get('eventid');
      this.event = database.getEvent(id);
      this.photos = database.getEventPhotos(id).map(array => array.reverse());
    });
    
  }
  
  takePicture() {
    this.camera.takePhoto().then(photo => {
      this.uploadPhoto(photo);
    });
  }
  
  choosePicture(){
    this.camera.choosePhoto().then(photo => {
      this.uploadPhoto(photo);
    });
  }
  
  viewPhoto(photo){
    var eventid = this.params.get('eventid');
    console.log('Viewing modal: ', { eventid: eventid, pictureid: photo.$key })
    let modal = this.modal.create(PicturePage, { eventid: eventid, pictureid: photo.$key });
    modal.present();
  }
  
  private uploadPhoto(photo:any){
          
      var photoEntity:any = { };
      photoEntity.created_date = photo.takenWhen;
      if (photo.fileUrl) photoEntity.file_url = photo.fileUrl;
      if (photo.dataUrl) photoEntity.photo_url = photo.dataUrl;
        
      var photoKey = this.photos.push(photoEntity).getKey();
      this.event.$ref.child('/num_photos').transaction(num_photos => num_photos + 1);
      
      var uploadTask = photo.file 
        ? this.storage.uploadBlob(this.event.$key, photoKey, photo.file) 
        : this.storage.uploadBase64(this.event.$key, photoKey, photo.data);
      
      uploadTask.then(snapshot => {
        
        this.photos.update(photoKey, { photo_url: snapshot.downloadURL });
        this.event.$ref.child('/photo_url').transaction(num_photos => snapshot.downloadURL);
        this.sharePhotosWithMembers(photoEntity, snapshot);
        
      });
  }
  
  private sharePhotosWithMembers(photoEntity, snapshot){
      this.event.members.forEach(member => {
        var message = this.messages.sharePhoto(member.eventId, snapshot.downloadURL, photoEntity.created_date);
        this.database.getMessages(member.uid).push(message);
      });
  }  
}
